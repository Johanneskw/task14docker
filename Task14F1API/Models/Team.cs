﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task14_F1_API.Models
{
    public class Team
    {
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Engine { get; set; }
        public List<Driver> Drivers { get; set; }
    }
}
