﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task14_F1_API.Models
{
    public class F1DbContext : DbContext
    {
        //Declaring the database schemas.
        public DbSet<Team> Teams { get; set; }
        public DbSet<Driver> Drivers { get; set; }

        //Setting Database migrate to run, so the database is migrated in the docker container. 
        public F1DbContext(DbContextOptions<F1DbContext> options) : base(options) {
            Database.Migrate();
        }

        protected override void OnModelCreating(ModelBuilder model)
        {
            //Seeding some data, first teams:
            model.Entity<Team>().HasData(new Team
            {
                TeamId = 1,
                Name = "Aston Martin RedBull Racing",
                Engine = "Honda"
            });
            model.Entity<Team>().HasData(new Team
            {
                TeamId = 2,
                Name = "Scuderia AlphaTauri Racing",
                Engine = "Honda"
            });
            model.Entity<Team>().HasData(new Team
            {
                TeamId = 3,
                Name = "Scuderia Ferrari ",
                Engine = "Ferrari"
            });
            model.Entity<Team>().HasData(new Team
            {
                TeamId = 4,
                Name = "McLaren F1 Team",
                Engine = "Renault"
            });
            model.Entity<Team>().HasData(new Team
            {
                TeamId = 5,
                Name = "Mercedes-AMG Petronas F1 Team",
                Engine = "Honda"
            });
            //Drivers:
            model.Entity<Driver>().HasData(new Driver
            {
                DriverID = 1,
                FirstName = "Max",
                LastName ="Verstappen",
                TeamID = 1
            });
            model.Entity<Driver>().HasData(new Driver
            {
                DriverID = 2,
                FirstName = "Alex",
                LastName = "Albon",
                TeamID = 1
            }); model.Entity<Driver>().HasData(new Driver
            {
                DriverID = 3,
                FirstName = "Danil",
                LastName = "Kvyat",
                TeamID = 2
            }); model.Entity<Driver>().HasData(new Driver
            {
                DriverID = 4,
                FirstName = "Pierre",
                LastName = "Gasly",
                TeamID = 2
            }); model.Entity<Driver>().HasData(new Driver
            {
                DriverID = 5,
                FirstName = "Sebastian",
                LastName = "Vettel",
                TeamID = 3
            }); model.Entity<Driver>().HasData(new Driver
            {
                DriverID = 6,
                FirstName = "Charles",
                LastName = "Leclerc",
                TeamID = 3
            }); model.Entity<Driver>().HasData(new Driver
            {
                DriverID = 7,
                FirstName = "Carlos",
                LastName = "Sainz Jr.",
                TeamID = 4
            }); model.Entity<Driver>().HasData(new Driver
            {
                DriverID = 8,
                FirstName = "Lando",
                LastName = "Norris",
                TeamID = 4
            }); model.Entity<Driver>().HasData(new Driver
            {
                DriverID = 9,
                FirstName = "Lewis",
                LastName = "Hamilton",
                TeamID = 5
            }); model.Entity<Driver>().HasData(new Driver
            {
                DriverID = 10,
                FirstName = "Valtteri",
                LastName = "Bottas",
                TeamID = 5
            });
        }
    }
}
