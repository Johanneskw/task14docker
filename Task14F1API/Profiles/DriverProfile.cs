﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task14_F1_API.DTOs.DriverDTOs;
using Task14_F1_API.Models;

namespace Task14_F1_API.Profiles
{
    public class DriverProfile : Profile
    {
        public DriverProfile()
        {
            //Mapping team names to the driverDTO, for display in the custom API method. 
            CreateMap<Driver, DriverDto>().ForMember(d => d.Team, opt => opt.MapFrom(t => t.Team.Name));
        }
    }
}
