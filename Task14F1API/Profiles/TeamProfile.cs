﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task14_F1_API.DTOs.DriverDTOs;
using Task14_F1_API.DTOs.TeamDTOs;
using Task14_F1_API.Models;

namespace Task14_F1_API.Profiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            //Mapping driverDTO to TeamDto.
            CreateMap<Team, TeamDto>().ReverseMap();
            CreateMap<TeamDto, DriverDto>().ReverseMap();
        }
    }
}
