﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task14_F1_API.DTOs.DriverDTOs;

namespace Task14_F1_API.DTOs.TeamDTOs
{
    public class TeamDto
    {
        //Simple team DTO, only relevant data + driverDto
        public int TeamID { get; set; }
        public string Name { get; set; }
        public string Engine { get; set; }
        public DriverDto Drivers { get; set; }
    }
}
