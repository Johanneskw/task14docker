﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task14_F1_API.DTOs.DriverDTOs
{
    public class DriverDto
    {
        //Simple driver DTO, only relevant data
        public int DriverID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Team { get; set; }
    }
}
