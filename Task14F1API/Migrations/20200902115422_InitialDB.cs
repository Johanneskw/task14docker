﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Task14F1API.Migrations
{
    public partial class InitialDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    TeamId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Engine = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.TeamId);
                });

            migrationBuilder.CreateTable(
                name: "Drivers",
                columns: table => new
                {
                    DriverID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    TeamID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Drivers", x => x.DriverID);
                    table.ForeignKey(
                        name: "FK_Drivers_Teams_TeamID",
                        column: x => x.TeamID,
                        principalTable: "Teams",
                        principalColumn: "TeamId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "TeamId", "Engine", "Name" },
                values: new object[,]
                {
                    { 1, "Honda", "Aston Martin RedBull Racing" },
                    { 2, "Honda", "Scuderia AlphaTauri Racing" },
                    { 3, "Ferrari", "Scuderia Ferrari " },
                    { 4, "Renault", "McLaren F1 Team" },
                    { 5, "Honda", "Mercedes-AMG Petronas F1 Team" }
                });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "DriverID", "FirstName", "LastName", "TeamID" },
                values: new object[,]
                {
                    { 1, "Max", "Verstappen", 1 },
                    { 2, "Alex", "Albon", 1 },
                    { 3, "Danil", "Kvyat", 2 },
                    { 4, "Pierre", "Gasly", 2 },
                    { 5, "Sebastian", "Vettel", 3 },
                    { 6, "Charles", "Leclerc", 3 },
                    { 7, "Carlos", "Sainz Jr.", 4 },
                    { 8, "Lando", "Norris", 4 },
                    { 9, "Lewis", "Hamilton", 5 },
                    { 10, "Valtteri", "Bottas", 5 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Drivers_TeamID",
                table: "Drivers",
                column: "TeamID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Drivers");

            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
