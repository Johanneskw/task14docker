Project contains a simple API, with data from an database created with Entity Frameworks. The API and database has been uploaded as an Image to docker Hub, and can be found at https://hub.docker.com/repository/docker/johanneskw/formula1api 

Task: 
create a simple .NET Core Web API with EF.
Docker requirements:

Create a Dockerfile to replicate your API
Use docker-compose to have a container of SQL Server in the container.
Build and run it locally to ensure it is working as intended. 
Place the image on dockerhub.